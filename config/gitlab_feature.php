<?php

return [

  /**
   * The name of the environment as you selected
   * in your feature flag strategies.
   *
   * See https://docs.gitlab.com/ee/operations/feature_flags.html#get-access-credentials
   */
  'name' => env('GITLAB_ENV_NAME', 'development'),

  /**
   * The instance id for your project shown on the
   * GitLab **Configure feature flags** page.
   *
   * See https://docs.gitlab.com/ee/operations/feature_flags.html#get-access-credentials
   */
  'instance_id' => env('GITLAB_INSTANCE_ID'),

  /**
   * The API URL where the client connects to
   * to get the list of feature flags.
   *
   * See https://docs.gitlab.com/ee/operations/feature_flags.html#get-access-credentials
   */
  'api_url' => env('GITLAB_API_URL'),

  /**
   * Disabling the verification may resolve problems
   * on platforms that don’t support SSL verification
   * or GitLab instances with invalid certificates.
   *
   * Be careful! This can cause security issues because
   * if enabled, it will establish a connection to your
   * GitLab instance, even if the connection could be
   * insecure.
   */
  'disable_verification' => env('GITLAB_DISABLE_VERIFICATION', false),

  /**
   * Feature flag responses from the GitLab instance are
   * cached to speed up individual site loads and prevent
   * relaying a potential attack of your app to your
   * GitLab instance.
   *
   * The cache duration is in minutes.
   */
  'cache_duration' => env('GITLAB_CACHE_DURATION', 5),
];
