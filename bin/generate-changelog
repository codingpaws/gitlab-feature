#!/usr/bin/env node
const { createInterface } = require("readline");
const readline = createInterface({
  input: process.stdin,
  output: process.stdout,
});
const { request } = require("https");
const { readFileSync, writeFileSync } = require("fs");

readline.question("milestone > ", (milestone) => {
  readline.close();
  const req = request(
    {
      hostname: "gitlab.com",
      port: 443,
      path: `/api/v4/projects/28361162/merge_requests?per_page=100&state=merged&milestone=${encodeURIComponent(
        milestone
      )}`,
      method: "GET",
    },
    (res) => {
      let raw = [];

      res.on("data", (d) => {
        raw.push(d.toString());
      });

      res.on("close", () => {
        const mergeRequests = JSON.parse(raw.join("")).filter(
          (mr) => !mr.labels.includes("tooling")
        );

        processMergeRequests(mergeRequests, milestone);
      });
    }
  );

  req.on("error", (error) => {
    throw error;
  });

  req.end();
});

const groupLabels = [
  "bug",
  "feature",
  "improvement",
  "security",
  "documentation",
  "other",
];

const processMergeRequests = (mergeRequests, milestone) => {
  const groups = {};
  groupLabels.forEach((label) => (groups[label] = []));

  for (const mr of mergeRequests) {
    const debugTitle = `${mr.references.relative} (${
      mr.title
    }; labels=${mr.labels.join(", ")})`;

    let hasGroup = false;
    for (const label of groupLabels) {
      if (mr.labels.includes(label)) {
        groups[label].push(mr);
        hasGroup = true;
        console.log(`[ ok ] ${debugTitle}`);
        break;
      }
    }

    if (!hasGroup) {
      console.log(`[skip] ${debugTitle}`);
    }
  }

  const markdown = [];

  for (const label in groups) {
    let title = label.substring(0, 1).toUpperCase() + label.substring(1);

    if (title === "Bug") {
      title = "Fixes";
    }

    const mrs = groups[label];

    if (mrs.length === 0) {
      continue;
    }

    const mrLinks = mrs
      .map((mr) => `- ${mr.title} ([${mr.references.relative}](${mr.web_url}))`)
      .join("\n");

    markdown.push(`### ${title}\n${mrLinks}`);
  }

  if (markdown.length === 0) {
    console.log(`No entries for ${milestone}`);
    return;
  }

  markdown.unshift(`## Release ${milestone}`);

  const changelog = readFileSync("CHANGELOG.md").toString();

  writeFileSync("CHANGELOG.md", markdown.join("\n") + "\n" + changelog);

  console.log(`\n✅ Added changelog entries for release ${milestone}`);
  console.log("💡 Remember to save with Prettier!");
};
