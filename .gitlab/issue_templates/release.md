<!--
  Replace %MILESTONE with the milestone of the
  tracked release, for example: %1.5
-->

When %MILESTONE is due:

1. [ ] Move all incomplete issues to the next release milestone
1. [ ] Create the new release via GitLab
1. [ ] Apply the ~21529029 label to all released issues in the milestone
1. [ ] Close the milestone
1. [ ] Post the release [on Twitter](https://twitter.com/codingpa_ws)

/label ~release
/assign @KevSlashNull
/milestone %MILESTONE
