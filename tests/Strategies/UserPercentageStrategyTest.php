<?php

namespace Tests\Strategies;

use CodingPaws\GitLabFeature\Strategies\UserPercentageStrategy;
use CodingPaws\GitLabFeature\UserIdResolver;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class UserPercentageStrategyTest extends TestCase
{

  public function testName()
  {
    $strategy = new UserPercentageStrategy();

    $this->assertEquals('gradualRolloutUserId', $strategy->name());
  }

  public function test100Percent()
  {
    $strategy = new UserPercentageStrategy(['percentage' => 100]);

    $this->assertTrue($strategy->check(), 'the strategy should be enabled');
  }


  public function test0Percent()
  {
    $strategy = new UserPercentageStrategy(['percentage' => 0]);

    $this->assertFalse($strategy->check(), 'the strategy should be disabled');
  }

  public function testSuccessfulCheck()
  {
    $strategy = new UserPercentageStrategy(['percentage' => 50]);

    Auth::shouldReceive('id')->andReturn(10);

    $this->assertTrue($strategy->check(), 'the strategy should be enabled');
  }

  public function testUnsuccessfulCheck()
  {
    $strategy = new UserPercentageStrategy(['percentage' => 50]);

    Auth::shouldReceive('id')->andReturn(35);

    $this->assertTrue($strategy->check(), 'the strategy should be disabled');
  }

  // public function testUnsuccessfulCheck()
  // {
  //   $strategy = new IdStrategy(['userIds' => '1,2,3']);

  //   Auth::shouldReceive('id')->andReturn(5);

  //   $this->assertFalse($strategy->check(), 'the strategy should be disabled');
  // }
}

class UserPercentageStrategyTestResolver extends UserIdResolver
{
  function resolve(): int|string|null
  {
    return Auth::id();
  }
}
