<?php

namespace Tests\Strategies;

use CodingPaws\GitLabFeature\Strategies\IdStrategy;
use CodingPaws\GitLabFeature\UserIdResolver;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class IdStrategyTest extends TestCase
{
  protected function setUp(): void
  {
    parent::setUp();
    IdStrategyTestResolver::register(new IdStrategyTestResolver);
  }

  public function testName()
  {
    $strategy = new IdStrategy();

    $this->assertEquals('userWithId', $strategy->name());
  }

  public function testSuccessfulCheck()
  {
    $strategy = new IdStrategy(['userIds' => '1,2,3']);

    Auth::shouldReceive('id')->andReturn(2);

    $this->assertTrue($strategy->check(), 'the strategy should be enabled');
  }

  public function testUnsuccessfulCheck()
  {
    $strategy = new IdStrategy(['userIds' => '1,2,3']);

    Auth::shouldReceive('id')->andReturn(5);

    $this->assertFalse($strategy->check(), 'the strategy should be disabled');
  }

  public function testEmptyUserIds()
  {
    $strategy = new IdStrategy(['userIds' => '']);

    Auth::shouldReceive('id')->andReturn(2);

    $this->assertFalse($strategy->check(), 'the strategy should be disabled');
  }
}

class IdStrategyTestResolver extends UserIdResolver
{
  function resolve(): int|string|null
  {
    return Auth::id();
  }
}
