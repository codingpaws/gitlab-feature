<?php

namespace Tests;

use CodingPaws\GitLabFeature\Feature;

class MockedFeatureTest extends TestCase
{
  public function testEnabled()
  {
    $mock = Feature::shouldReceive('enabled')
      ->with('test_flag')
      ->andReturn(true);

    $this->assertTrue(Feature::enabled('test_flag'));
    $mock->verify();
  }

  public function testDisabled()
  {
    $mock = Feature::shouldReceive('disabled')
      ->with('test_flag', false)
      ->andReturn(true);

    $this->assertTrue(Feature::disabled('test_flag', false));
    $mock->verify();
  }
}
