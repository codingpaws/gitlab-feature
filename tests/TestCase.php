<?php

namespace Tests;

use CodingPaws\GitLabFeature\FeatureServiceProvider;
use CodingPaws\GitLabFeature\UserIdResolver;
use Illuminate\Support\Facades\Config;
use Orchestra\Testbench\TestCase as TestbenchTestCase;
use ReflectionClass;

abstract class TestCase extends TestbenchTestCase
{
  protected function getPackageProviders($app)
  {
    return [FeatureServiceProvider::class];
  }

  protected function setUp(): void
  {
    parent::setUp();

    $this->resetUserIdResolver();
  }

  protected function mockConfig(string $key, mixed $value): void
  {
    Config::shouldReceive('get')
      ->with($key)
      ->andReturn($value);
  }

  private function resetUserIdResolver()
  {
    $class = new ReflectionClass(UserIdResolver::class);
    $class->setStaticPropertyValue('resolver', null);
  }
}
