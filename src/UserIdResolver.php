<?php

namespace CodingPaws\GitLabFeature;

use Illuminate\Support\Facades\Auth;

abstract class UserIdResolver
{
  private static ?UserIdResolver $resolver = null;

  public static function register(UserIdResolver $resolver): void
  {
    static::$resolver = $resolver;
  }

  public static function id(): int|string|null
  {
    if (is_null(static::$resolver)) {
      return Auth::id();
    }

    return static::$resolver->resolve();
  }

  /**
   * Returns the user ID of the current user
   * or `null` when no user is logged in.
   *
   * @see `Illuminate\Support\Facades\Auth::id()`
   */
  abstract public function resolve(): int|string|null;
}
