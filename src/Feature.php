<?php

namespace CodingPaws\GitLabFeature;

use Illuminate\Support\Facades\Facade;

/**
 * @method static array all() Returns an array (`string` => `bool`) of all
 *         feature flags and whether they are enabled.
 * @method static bool enabled(string $name, bool $default = false) Returns
 *         true if the feature flag `$name` is enabled, otherwise returns
 *         the `$default`.
 * @method static bool disabled(string $name, bool $default = false) Returns
 *         true if the feature flag `$name` is not enabled, otherwise
 *         returns the `$default`.
 * @method static void refresh() Clears and repopulates the feature flag
 *         cache.
 */
class Feature extends Facade
{
  protected static function getFacadeAccessor(): string
  {
    return 'gitlab_feature';
  }
}
