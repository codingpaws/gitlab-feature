<?php

namespace CodingPaws\GitLabFeature\Middlewares;

use Closure;
use CodingPaws\GitLabFeature\Feature;
use Illuminate\Http\Request;

class FeatureFlagMiddleware
{
  public function handle(Request $request, Closure $next, string $feature_name)
  {
    if (Feature::enabled($feature_name)) {
      return $next($request);
    }
    return abort(404);
  }
}
