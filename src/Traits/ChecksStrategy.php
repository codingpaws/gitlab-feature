<?php

namespace CodingPaws\GitLabFeature\Traits;

use CodingPaws\GitLabFeature\Strategies\Base\StrategyResolver;

trait ChecksStrategy
{
  protected function checkStrategies(array $strategies): bool
  {
    foreach ($strategies as $strategy) {
      if ($this->checkStrategy($strategy)) {
        return true;
      }
    }

    return false;
  }

  private function checkStrategy(array $strategy): bool
  {
    $rollout_strategy = StrategyResolver::resolve($strategy['name'], $strategy['parameters'] ?? []);

    return $rollout_strategy->check();
  }
}
