<?php

namespace CodingPaws\GitLabFeature\Exceptions;

use Exception;

class UnknownStrategyException extends Exception
{
  public function __construct(string $strategy_name)
  {
    parent::__construct("{$strategy_name} is not a valid strategy.");
  }
}
