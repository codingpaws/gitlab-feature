<?php

namespace CodingPaws\GitLabFeature\Exceptions;

use CodingPaws\GitLabFeature\Strategies\Base\Strategy;
use Exception;

class ClassDoesntExtendStrategyException extends Exception
{
  public function __construct(string $class)
  {
    $strategy = Strategy::class;

    parent::__construct("{$class} doesn’t extend the {$strategy} class.");
  }
}
