<?php

namespace CodingPaws\GitLabFeature\Strategies\Base;

use CodingPaws\GitLabFeature\UserIdResolver;
use Illuminate\Support\Facades\Session;

/**
 * A rollout strategy used to determine
 * whether the feature flag using this
 * strategy is rolled out by it.
 */
abstract class Strategy
{
  private array $parameters;

  public function __construct(array $parameters = [])
  {
    $this->parameters = $parameters;
  }

  /**
   * The name of the strategy used to
   * compare with the feature flag strategy
   * name.
   */
  abstract public function name(): string;

  /**
   * Check whether the feature flag using this
   * strategy is rolled out.
   */
  abstract public function check(): bool;

  protected function parameters(): array
  {
    return $this->parameters;
  }

  protected function getUserBasedPercentage(): ?int
  {
    if (is_null(UserIdResolver::id())) {
      return null;
    }

    $id = UserIdResolver::id() * ord($this->name());

    return $id % 100;
  }

  protected function getSessionBasedPercentage(): int
  {
    $i = 0;

    foreach (str_split(Session::getId()) as $char) {
      $i += ord($char);
    }

    return $i % 100;
  }

  protected function getAnyRolloutPercentage(): int
  {
    return $this->getUserBasedPercentage() ?? $this->getSessionBasedPercentage();
  }
}
