<?php

namespace CodingPaws\GitLabFeature\Strategies\Base;

use CodingPaws\GitLabFeature\Exceptions\ClassDoesntExtendStrategyException;
use CodingPaws\GitLabFeature\Exceptions\UnknownStrategyException;
use ReflectionClass;

final class StrategyResolver
{
  private static array $strategies = [];

  public static function register(string $strategyClass): void
  {
    if (!in_array(Strategy::class, class_parents($strategyClass))) {
      throw new ClassDoesntExtendStrategyException($strategyClass);
    }
    $class = new ReflectionClass($strategyClass);

    $strategy = $class->newInstance();

    self::$strategies[$strategy->name()] = $class;
  }

  public static function resolve(string $name, array $parameters): Strategy
  {
    if (!array_key_exists($name, self::$strategies)) {
      throw new UnknownStrategyException($name);
    }

    return self::$strategies[$name]->newInstance($parameters);
  }
}
