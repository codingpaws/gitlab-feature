<?php

namespace CodingPaws\GitLabFeature\Strategies;

use CodingPaws\GitLabFeature\Strategies\Base\Strategy;

class FlexibleRolloutStrategy extends Strategy
{
  private const STICKINESS_DEFAULT = 'default';
  private const STICKINESS_SESSION = 'sessionid';
  private const STICKINESS_USER = 'userid';
  private const STICKINESS_RANDOM = 'random';

  public function name(): string
  {
    return 'flexibleRollout';
  }

  public function check(): bool
  {
    $percentage = $this->getPercentage();

    if (is_null($percentage)) {
      return false;
    }

    $max_percentage = (int) $this->parameters()['rollout'];

    return $max_percentage > 0 && $percentage <= $max_percentage;
  }

  public function getStickiness(): string
  {
    return strtolower($this->parameters()['stickiness']);
  }

  private function getPercentage(): ?int
  {
    return match ($this->getStickiness()) {
      self::STICKINESS_SESSION => $this->getSessionBasedPercentage(),
      self::STICKINESS_USER => $this->getUserBasedPercentage(),
      self::STICKINESS_DEFAULT => $this->getDefaultPercentage(),
      self::STICKINESS_RANDOM => $this->randomPercentage(),
      default => null,
    };
  }

  private function randomPercentage(): float
  {
    return random_int(0, 100);
  }

  private function getDefaultPercentage()
  {
    $user_percentage = $this->getUserBasedPercentage();

    if (is_null($user_percentage)) {
      return $this->getSessionBasedPercentage();
    }

    return $user_percentage;
  }
}
