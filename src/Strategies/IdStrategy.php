<?php

namespace CodingPaws\GitLabFeature\Strategies;

use CodingPaws\GitLabFeature\Strategies\Base\Strategy;
use CodingPaws\GitLabFeature\UserIdResolver;

class IdStrategy extends Strategy
{
  public function name(): string
  {
    return 'userWithId';
  }

  public function check(): bool
  {
    return $this->isCurrentUserTargetedByStrategy();
  }

  private function isCurrentUserTargetedByStrategy()
  {
    $userIds = $this->getTargetedUserIds();

    return in_array(UserIdResolver::id(), $userIds);
  }

  private function getTargetedUserIds()
  {
    return explode(',', $this->parameters()['userIds'] ?: '');
  }
}
