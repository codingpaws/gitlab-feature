<?php

namespace CodingPaws\GitLabFeature\Strategies;

use CodingPaws\GitLabFeature\Strategies\Base\Strategy;

class UserPercentageStrategy extends Strategy
{
  public function name(): string
  {
    return 'gradualRolloutUserId';
  }

  public function check(): bool
  {
    $percentage = $this->getAnyRolloutPercentage();

    $max_percentage = $this->parameters()['percentage'];

    return $max_percentage > 0 && $percentage <= $max_percentage;
  }
}
