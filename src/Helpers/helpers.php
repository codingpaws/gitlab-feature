<?php

use CodingPaws\GitLabFeature\Feature;

if (!function_exists('feature_enabled')) {
  /**
   * Returns true if the feature $name
   * is enabled or it’s not present
   * and the default is true.
   */
  function feature_enabled(string $name, bool $default = false): bool
  {
    return Feature::enabled($name, $default);
  }
}

if (!function_exists('feature_disabled')) {
  /**
   * Returns true if the feature $name
   * is disable or it’s not present
   * and the default is true.
   */
  function feature_disabled(string $name, bool $default = false): bool
  {
    return Feature::disabled($name, $default);
  }
}
