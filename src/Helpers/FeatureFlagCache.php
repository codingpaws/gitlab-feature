<?php

namespace CodingPaws\GitLabFeature\Helpers;

use Carbon\Carbon;
use CodingPaws\GitLabFeature\Base\GitLabLoader;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

final class FeatureFlagCache
{
  public const BASE_CACHE_KEY = 'gitlab_feature::features';

  public static function get(?GitLabLoader $service)
  {
    return Cache::get(self::getCacheKey(), static fn () => self::loadFeatures($service));
  }

  /**
   * Clear the cache and force reloading
   * when FeatureFlagCache::get() is called.
   */
  public static function clear(): void
  {
    Cache::forget(self::getCacheKey());
  }

  /**
   * Get the environment-specific cache key
   * to avoid cross environment inconsistencies.
   */
  public static function getCacheKey(): string
  {
    return self::BASE_CACHE_KEY . '::' . Config::get('gitlab_feature.name');
  }

  private static function loadFeatures(?GitLabLoader $service)
  {
    if (!$service) {
      return [];
    }

    $features = $service->load();

    self::set($features);

    return $features;
  }

  private static function set(array $features): void
  {
    Cache::put(self::getCacheKey(), $features, self::cacheDuration());
  }

  private static function cacheDuration(): Carbon
  {
    $fallback = App::environment('local', 'development') ? 5 : 30;
    $duration = (int) Config::get('gitlab_feature.cache_duration') ?: $fallback;

    return Carbon::now()->addMinutes($duration);
  }
}
