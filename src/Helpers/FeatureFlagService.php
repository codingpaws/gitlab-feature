<?php

namespace CodingPaws\GitLabFeature\Helpers;

use CodingPaws\GitLabFeature\Base\GitLabLoader;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Throwable;

class FeatureFlagService implements GitLabLoader
{
  public function load(): array
  {
    if (!defined('CODINGPAWS_TESTS_RUNNING') && App::runningUnitTests()) {
      return [];
    }

    try {
      $response = $this->performRequest();

      $statusCode = $response->status();

      if ($statusCode >= 200 && $statusCode < 300) {
        $body = $response->json() ?? [];
        return $body['features'] ?? [];
      }
    } catch (Throwable $th) {
      $this->handleError($th);
    }
    return [];
  }

  private function handleError(Throwable $th): void
  {
    if (App::runningUnitTests()) {
      throw $th;
    } else {
      error_log($th->getMessage());
    }
  }

  private function performRequest()
  {
    return Http::withOptions($this->options())
      ->withHeaders($this->headers())
      ->timeout(1)
      ->get($this->getURL());
  }

  /**
   * SSL verification likely not works on Windows
   * or in local environments.
   */
  private function shouldVerify(): bool
  {
    return !Config::get('gitlab_feature.disable_verification');
  }

  private function headers(): array
  {
    return [
      'UNLEASH-APPNAME' => Config::get('gitlab_feature.name'),
      'UNLEASH-INSTANCEID' => Config::get('gitlab_feature.instance_id'),
    ];
  }

  private function options(): array
  {
    return [
      'verify' => $this->shouldVerify(),
    ];
  }

  private function getURL(): string
  {
    $url = Str::of(Config::get('gitlab_feature.api_url'));

    if (!$url->endsWith('/client/features')) {
      $url = $url->rtrim('/')->append('/client/features');
    }

    return $url->__toString();
  }
}
