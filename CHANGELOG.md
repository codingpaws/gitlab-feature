## Release 1.5

### Improvement

- Add cache refresh method ([!51](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/51))
- Document methods on Feature facade ([!50](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/50))

### Documentation

- Add image for Configure button to Getting Started ([!48](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/48))

## Release 1.4

### Fixes

- Use int-based percentage for "random" stickiness ([!43](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/43))
- Add support for all GitLab stickinesses of flexible rollout ([!39](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/39))

### Feature

- Add "all" method to Feature facade ([!45](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/45))

### Security

- Update Laravel dependency to apply CVE-2021-43808 fix ([!41](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/41))

### Documentation

- Document Feature::all method in README ([!47](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/47))

## Release 1.3

### Fixes

- Fix flexible rollout uses wrong parameter ([!38](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/38))
- Fix stickiness is wrongly detected ([!37](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/37))

### Improvement

- Add some missing type annotations ([!34](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/34))
- Remove Feature::mock ([!28](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/28))
- Improve feature flag cache coverage ([!27](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/27))
- Make "Feature" a Laravel facade ([!24](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/24))

### Security

- Add test for FeatureFlagMiddleware ([!32](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/32))

### Documentation

- Document configurable cache duration ([!36](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/36))
- Document how to mock feature flags (Feature facade) ([!31](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/31))

## Release 1.2.3

### Fixes

- Cache depending on environment ([!30](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/30))

## Release 1.2.2

### Fixes

- Fix no feature flags are loaded ([!29](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/29))

## Release 1.2

### Fixes

- Prevent loading from GitLab in non codingpaws tests ([!26](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/26))

### Feature

- Add user ID resolver ([!20](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/20))
- Add feature flag middleware ([!17](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/17))

### Improvement

- Use clearer "register" to register a new resolver ([!23](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/23))
- Make SSL verification configurable ([!19](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/19))

## Release 1.1

### Fixes

- Append correct endpoint for loading feature flags when not present ([!10](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/10))
- Add missing import in loading service ([!6](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/6))
- Declutter Feature class ([!4](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/4))

### Improvement

- Allow to clear the feature flag cache ([!12](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/12))

### Documentation

- Document caching in README ([!13](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/13))
- Add helper usage to README ([!11](https://gitlab.com/codingpaws/gitlab-feature/-/merge_requests/11))
